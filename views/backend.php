<?php

require("/controllers/FrontController.php");
require("/models/Folder.php");
require("/models/File.php");
require("/views/View.php");

$frontController = new FrontController();
$views = new View();
$autorizeObj = new Autorize();
#$content = scandir(__DIR__);

if(isset($_POST["action"]) && $_POST["action"] == "openFolder"){	$folderName = $_POST["folder-name"];
	$frontController->openFolder($folderName);
} else if(isset($_POST["action"]) && $_POST["action"] == "openFile") { 	$fileName = $_POST["file-name"];
 	$frontController->openFile($fileName);
}

